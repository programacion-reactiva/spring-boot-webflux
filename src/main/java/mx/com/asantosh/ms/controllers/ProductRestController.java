package mx.com.asantosh.ms.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.com.asantosh.ms.beans.client.products.Products;
import mx.com.asantosh.ms.services.IProductService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/app/product")
public class ProductRestController {
		@Autowired 
		private IProductService productService;
		
		@GetMapping("/listar")
		public Flux<Products> listar(Model model) {
			return productService.findAll();
		}
		
		@GetMapping("/listar/{id}")
		public Mono<Products>  listarDataDriver(@PathVariable String id){
			
			return productService.findById(id);
		}
		
}
