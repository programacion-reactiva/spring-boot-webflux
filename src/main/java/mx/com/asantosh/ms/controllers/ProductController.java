package mx.com.asantosh.ms.controllers;

import java.io.File;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.thymeleaf.spring6.context.webflux.ReactiveDataDriverContextVariable;

import jakarta.validation.Valid;

import mx.com.asantosh.ms.beans.client.products.Categoria;
import mx.com.asantosh.ms.beans.client.products.Products;
import mx.com.asantosh.ms.services.IProductService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SessionAttributes("product")
@Controller
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private IProductService productService;
	
	@Value("${config.upload.path}")
	private String path;

	@ModelAttribute("categories")
	public Flux<Categoria> categorias() {
		return productService.findAllCategoria();
	}

	@GetMapping("/listar")
	public String listar(Model model) {
		Flux<Products> products = productService.findAll();

		model.addAttribute("title", "Listado de productos");
		model.addAttribute("products", products);
		return "listar";
	}

	@GetMapping("/listar-datadriver")
	public String listarDataDriver(Model model) {
		Flux<Products> products = productService.findAllDelay();

		model.addAttribute("title", "Listado de productos");
		model.addAttribute("products", new ReactiveDataDriverContextVariable(products, 3));
		return "listar";
	}

	@GetMapping("/listar-full")
	public String listarFull(Model model) {
		Flux<Products> products = productService.findAllRepeat();

		model.addAttribute("title", "Listado de productos");
		model.addAttribute("products", new ReactiveDataDriverContextVariable(products, 3));
		return "listar";
	}

	@GetMapping("/listar-chunked")
	public String listarChunked(Model model) {
		Flux<Products> products = productService.findAllRepeat();

		model.addAttribute("title", "Listado de productos");
		model.addAttribute("products", new ReactiveDataDriverContextVariable(products, 3));
		return "listar-chunked";
	}

	@GetMapping("/form")
	public Mono<String> crear(Model model) {
		model.addAttribute("product", new Products());
		model.addAttribute("titulo", "Formulario de producto");
		return Mono.just("form");
	}

	@GetMapping("/form/{id}")
	public Mono<String> editar(@PathVariable String id, Model model) {
		Mono<Products> productMono = productService.findById(id);
		model.addAttribute("title", "Edit Products");
		model.addAttribute("product", productMono);
		return Mono.just("form");
	}

	@PostMapping("/form")
	public Mono<String> agregar(@Valid @ModelAttribute("product") Products product, BindingResult result,
			@RequestPart FilePart file, SessionStatus status) {
		if (result.hasErrors()) {
			return Mono.just("form");
		} else {
			status.setComplete();

			Mono<Categoria> monoCategoria = productService.findCategoriaById(product.getCategory().getId());
			return monoCategoria.flatMap(c -> {
				if (product.getCreateAt() == null) {
					product.setCreateAt(new Date());
				}
				if (!file.filename().isEmpty()) {
					product.setPhoto(UUID.randomUUID().toString() + "-"
							+ file.filename().replace(" ", "").replace(":", "").replace("\\", ""));
				}
				product.setCategory(c);
				return productService.save(product);
			}).flatMap(p -> {
				if (!file.filename().isEmpty()) {
					return file.transferTo(new File(path+p.getPhoto()));
				}
				return Mono.empty();
			}).thenReturn("redirect:/product/listar");
		}
	}

	@GetMapping("/eliminar/{id}")
	public Mono<String> eliminar(@PathVariable String id) {
		return productService.findById(id).defaultIfEmpty(new Products()).flatMap(p -> {
			if (p.getId() == null) {
				return Mono.error(new InterruptedException("No existe el producto a eliminar"));
			}
			return Mono.just(p);
		}).flatMap(p -> {
			return productService.delete(p);
		}).then(Mono.just("redirect:/product/listar"));

	}
	
	@GetMapping("/ver/{id}")
	public Mono<String> ver(Model model,@PathVariable String id){
		return productService.findById(id)
				.doOnNext(p -> {
					model.addAttribute("product",p);
					model.addAttribute("title","Detalle");
				}).switchIfEmpty(Mono.just(new Products()))
				.flatMap(p -> {
					if (p.getId() == null) {
						return Mono.error(new InterruptedException("No existe el producto a eliminar"));
					}
					return Mono.just(p);
					
				}).then(Mono.just("ver"));
	}
	
	@GetMapping("/uploads/img/{nombreFoto:.+}")
	public Mono<ResponseEntity<Resource>> verFoto(@PathVariable String nombreFoto) throws MalformedURLException{
		Path ruta = Paths.get(path).resolve(nombreFoto).toAbsolutePath();
		Resource imagen = new UrlResource(ruta.toUri());
		return Mono.just(
				ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachments; filename=\""+imagen.getFilename()+"\"")
				.body(imagen));
		
	}
}
