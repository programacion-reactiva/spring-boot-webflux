package mx.com.asantosh.ms.beans.client.products;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Document("categories")
public class Categoria implements Serializable {
	
	private static final long serialVersionUID = -4208044334118082051L;
	@NotEmpty
	@Id
	private String id;
	private String name;
	
	public Categoria(String name) {
		this.name = name;
	}
	
	
	
}