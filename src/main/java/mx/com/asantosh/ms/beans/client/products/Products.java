package mx.com.asantosh.ms.beans.client.products;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Document(collection = "products")
public class Products implements Serializable {

	private static final long serialVersionUID = 1574965306220541068L;

	@Id
	private String id;
	@NotEmpty
	private String name;
	@NotNull
	private Double price;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date createAt;
	@Valid
	private Categoria category;
	
	private String photo;

	public Products(String name, Double price) {
		this.name = name;
		this.price = price;
	}
	
	public Products(String name, Double price,Categoria category) {
		this.name = name;
		this.price = price;
		this.category = category;
	}

}
