package mx.com.asantosh.ms.beans.products.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import mx.com.asantosh.ms.beans.client.products.Categoria;

public interface CategoriaDao extends ReactiveMongoRepository<Categoria,String> {

}
