package mx.com.asantosh.ms.beans.products.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import mx.com.asantosh.ms.beans.client.products.Products;

public interface ProductDao extends ReactiveMongoRepository<Products,String>{

}
