package mx.com.asantosh.ms;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

import mx.com.asantosh.ms.beans.client.products.Categoria;
import mx.com.asantosh.ms.beans.client.products.Products;
import mx.com.asantosh.ms.services.IProductService;
import reactor.core.publisher.Flux;

@SpringBootApplication
public class SpringBootWebfluxApplication implements CommandLineRunner {
	
	private static Logger logger = LoggerFactory.getLogger(SpringBootWebfluxApplication.class);
	@Autowired
	private IProductService pruductService;
	@Autowired
	private ReactiveMongoTemplate mongoTemplate;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebfluxApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		mongoTemplate.dropCollection("products").subscribe();
		mongoTemplate.dropCollection("categories").subscribe();
		
		Categoria electronico =  new Categoria("Electronico");
		Categoria deporte =  new Categoria("Deporte");
		Categoria computo =  new Categoria("Computo");
		Categoria muebles =  new Categoria("Muebles");
		Categoria despensa = new Categoria("Despensa");
		
		Flux.just(electronico,deporte,computo,muebles,despensa)
		.flatMap(cat -> pruductService.saveCategoria(cat))
		.thenMany(
				Flux.just(new Products("Coca-cola 1L",19.50, despensa),
						new Products("Papas sabritas",17.00,despensa),
						new Products("Jabon Roma 1kg",41.50,despensa),
						new Products("Jabon zote 400",21.00,despensa),
						new Products("Coca-cola .500L",15.0,despensa),
						new Products("PC Intel core i5",1000.0,computo),
						new Products("Wilson #5",1000.0,deporte),
						new Products("Cable Naranja 5m",1000.0,electronico),
						new Products("Escritorio ejecutivo",1000.0,muebles),
						new Products("Coca-cola .200L",10.0,despensa))
				.flatMap(product -> {
					product.setCreateAt(new Date());
					return pruductService.save(product);
					})
				)
		.subscribe(product -> logger.info("Insert: ".concat(product.getId()).concat(" ").concat(product.getName())));
		
	}

}
