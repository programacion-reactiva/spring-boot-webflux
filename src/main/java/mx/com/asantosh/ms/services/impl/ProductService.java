package mx.com.asantosh.ms.services.impl;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.asantosh.ms.beans.client.products.Categoria;
import mx.com.asantosh.ms.beans.client.products.Products;
import mx.com.asantosh.ms.beans.products.dao.CategoriaDao;
import mx.com.asantosh.ms.beans.products.dao.ProductDao;
import mx.com.asantosh.ms.services.IProductService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ProductService implements IProductService {

	@Autowired
	private ProductDao productDao;
	@Autowired
	private CategoriaDao categoriaDao;
	
	@Override
	public Flux<Products> findAll() {
		return productDao.findAll();
	}

	@Override
	public Mono<Products> findById(String id) {
		return productDao.findById(id);
	}

	@Override
	public Mono<Products> save(Products product) {
		return productDao.save(product);
	}

	@Override
	public Mono<Void> delete(Products product) {
		return productDao.delete(product);
	}

	@Override
	public Flux<Products> findAllNombreUpperCase() {
		return  productDao.findAll()
				.map(product -> {
					product.setName(product.getName().toUpperCase());
					return product;});
		
	}

	@Override
	public Flux<Products> findAllDelay() {
		return productDao.findAll()
				.map(product -> {
					product.setName(product.getName().toUpperCase());
					return product;})
				.delayElements(Duration.ofSeconds(1));
	}

	@Override
	public Flux<Products> findAllRepeat() {
		return productDao.findAll()
				.map(product -> {
					product.setName(product.getName().toUpperCase());
					return product;})
				.repeat(5000);
	}

	@Override
	public Flux<Categoria> findAllCategoria() {
		return categoriaDao.findAll();
	}

	@Override
	public Mono<Categoria> findCategoriaById(String id) {
		return categoriaDao.findById(id);
	}

	@Override
	public Mono<Categoria> saveCategoria(Categoria category) {
		return categoriaDao.save(category);
	}

}
