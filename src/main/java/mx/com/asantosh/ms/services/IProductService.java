package mx.com.asantosh.ms.services;

import mx.com.asantosh.ms.beans.client.products.Categoria;
import mx.com.asantosh.ms.beans.client.products.Products;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IProductService {
	
	public Flux<Products> findAll();
	
	public Flux<Products> findAllNombreUpperCase();
	public Flux<Products> findAllDelay();
	public Flux<Products> findAllRepeat();
	
	public Mono<Products> findById(String id);
	
	public Mono<Products> save(Products product);
	
	public Mono<Void> delete(Products product);
	
	public Flux<Categoria> findAllCategoria();
	public Mono<Categoria> findCategoriaById(String id);
	public Mono<Categoria> saveCategoria(Categoria Category);
}
